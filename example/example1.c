/* Example 1
 * This program shows how to use basic structures in graphlib.
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <graphlib/graph.h>

int main()
{
	Graph g;
	VerticeID v1, v2, v3;
	g = g_create_graph ();
	v1 = g_insert_vertice (g, NULL);
	v2 = g_insert_vertice (g, NULL);
	v3 = g_insert_vertice (g, NULL);

	g_insert_edge_undirected (g, v1, v3, NULL);
	g_insert_edge_undirected (g, v1, v2, NULL);
	g_insert_edge_undirected (g, v3, v2, NULL);
	g_insert_edge_directed (g, v2, v1, NULL);

	dump_graph (stderr, g);
	g_destroy_graph (g);
	return 0;
}
