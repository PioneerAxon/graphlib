/* Example 4
 * This program takes vertice and edge counts and then
 * inserts random weighted edges.
 */

#include <stdio.h>
#include <stdlib.h>

#include <graphlib/graph.h>
#include <graphlib/weighted_graph.h>


unsigned long int get_next (unsigned long int vertice_count)
{
	return (random () % vertice_count);
}

EdgeID insert_edge (Graph g, unsigned long int vertice_count)
{
	VerticeID v1, v2;
	v1 = get_next (vertice_count);
	v2 = get_next (vertice_count);
	if (get_next (2))
		return wg_insert_edge_directed (g, v1, v2, (((float)get_next (1000000))/1000000.0f));

	else
		return wg_insert_edge_undirected (g, v1, v2, (((float)get_next (1000000))/1000000.0f));
}

int main()
{
	Graph g;
	unsigned long int count;
	g = g_create_graph ();
	scanf ("%lu",&count);
	while (count--)
		g_insert_vertice (g, NULL);
	scanf ("%lu", &count);
	while (count--)
	{
		fprintf (stderr, "%f\n", wg_get_weight (g, insert_edge (g, g_vertice_count (g))));
	}
	dump_graph (stderr, g);
	g_destroy_graph (g);
	return 0;
}
