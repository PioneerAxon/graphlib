/* Example 3
 * This program takes two unsigned long int named vertice count and edge
 * count and generates the graph.
 * Useful for benchmarking.
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <graphlib/graph.h>

unsigned long int get_next (unsigned long int vertice_count)
{
	return (random () % vertice_count);
}

void insert_edge (Graph g, unsigned long int vertice_count)
{
	VerticeID v1, v2;
	v1 = get_next (vertice_count);
	v2 = get_next (vertice_count);
	if (get_next (2))
		g_insert_edge_directed (g, v1, v2, NULL);
	else
		g_insert_edge_undirected (g, v1, v2, NULL);
}


int main()
{
	Graph g;
	unsigned long int count;
	g = g_create_graph ();
	scanf ("%lu",&count);
	while (count--)
		g_insert_vertice (g, NULL);
	scanf ("%lu", &count);
	while (count--)
		insert_edge (g, g_vertice_count (g));
	dump_graph (stderr, g);
	g_destroy_graph (g);
	return 0;
}
