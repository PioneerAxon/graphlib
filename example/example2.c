/* Example 2
 * This program takes vertice count and then edge details 
 * (source and destination vertices) to generate graph.
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <graphlib/graph.h>

int main()
{
	Graph g;
	VerticeID v1, v2;
	unsigned long int count;
	g = g_create_graph ();
	scanf ("%lu",&count);
	while (count--)
		g_insert_vertice (g, NULL);
	while (scanf ("%lu %lu", &v1, &v2) != EOF)
		g_insert_edge_directed (g, v1, v2, NULL);
	dump_graph (stderr, g);
	g_destroy_graph (g);
	return 0;
}
