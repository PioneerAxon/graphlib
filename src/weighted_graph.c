/*
 * Copyright (C) Arth Patel (PioneerAxon) 2012 <arth.svnit@gmail.com>
 * See README file for licensing information.
 */

#include <stdlib.h>
#include <assert.h>

#include "graph.h"
#include "weighted_graph.h"

/* Set weight to already inserted edge. */
void wg_set_weight (Graph g, EdgeID e, WEIGHT_TYPE value)
{
	WEIGHT_TYPE* weight = (WEIGHT_TYPE *) malloc (sizeof (WEIGHT_TYPE));
	*weight = value;
	g_e_set_data (g, e, weight);
}

/* Get weight from an edge. */
WEIGHT_TYPE wg_get_weight (Graph g, EdgeID e)
{
	assert (g_e_get_data (g, e));
	return (*(WEIGHT_TYPE *)g_e_get_data (g, e));
}

/* Insert edge in Graph g with weight w. */
EdgeID wg_insert_edge (Graph g, VerticeID v1, VerticeID v2, WEIGHT_TYPE w, EdgeType type)
{
	WEIGHT_TYPE* weight = (WEIGHT_TYPE *) malloc (sizeof (WEIGHT_TYPE));
	*weight = w;
       return g_insert_edge (g, v1, v2, weight, type);
}

/* Insert directed edge in Graph g with weight w. */
EdgeID wg_insert_edge_directed (Graph g, VerticeID from, VerticeID to, WEIGHT_TYPE w)
{
	WEIGHT_TYPE* weight = (WEIGHT_TYPE *) malloc (sizeof (WEIGHT_TYPE));
	*weight = w;
	return g_insert_edge_directed (g, from, to, weight);
}

/* Insert undirected edge in Graph g with weight w. */
EdgeID wg_insert_edge_undirected (Graph g, VerticeID v1, VerticeID v2, WEIGHT_TYPE w)
{
	WEIGHT_TYPE* weight = (WEIGHT_TYPE *) malloc (sizeof (WEIGHT_TYPE));
	*weight = w;
	return g_insert_edge_undirected (g, v1, v2, weight);
}
