/*
 * Copyright (C) Arth Patel (PioneerAxon) 2012 <arth.svnit@gmail.com>
 * See README file for licensing information.
 */

#ifndef __WEIGHTED_GRAPH_H_
#define __WEIGHTED_GRAPH_H_

#define WEIGHT_TYPE float

#include <stdlib.h>

#include "graph.h"

/* Weighted Graph manipulation functions. */
/* weighted_graph.c */
void wg_set_weight (Graph, EdgeID, WEIGHT_TYPE);
WEIGHT_TYPE wg_get_weight (Graph, EdgeID);
EdgeID wg_insert_edge (Graph, VerticeID, VerticeID, WEIGHT_TYPE, EdgeType);
EdgeID wg_insert_edge_directed (Graph, VerticeID, VerticeID, WEIGHT_TYPE);
EdgeID wg_insert_edge_undirected (Graph, VerticeID, VerticeID, WEIGHT_TYPE);

#endif /*__WEIGHTED_GRAPH_H_ */
