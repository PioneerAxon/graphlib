/*
 * Copyright (C) Arth Patel (PioneerAxon) 2012 <arth.svnit@gmail.com>
 * See README file for licensing information.
 */

#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#include "graph.h"

/* Get Vertice from VerticeID. */
static Vertice g_get_vertice (Graph g, VerticeID id)
{
	assert (g->vertices_count > id);

	return (&(g->vertices [id]));
}

/* Get Edge from EdgeID. */
static Edge g_get_edge (Graph g, EdgeID id)
{
	assert (g->edges_count > id);

	return (&(g->edges [id]));
}

/* Get indegree of vertice. */
_COUNT_TYPE g_v_indegree (Graph g, VerticeID id)
{
	return  g_get_vertice (g, id)->incoming_edge_count + g_get_vertice (g, id)->undirected_edge_count;
}

/* Get outdegree of vertice. */
_COUNT_TYPE g_v_outdegree (Graph g, VerticeID id)
{
	return g_get_vertice (g, id)->outgoing_edge_count + g_get_vertice (g, id)->undirected_edge_count;
}

/* Get array of incoming EdgeID(s). */
_ID_TYPE* g_v_incoming_edges (Graph g, VerticeID id)
{
	return (g_get_vertice (g, id)->incoming_edge_id);
}

/* Get array of outgoing EdgeID(s). */
_ID_TYPE* g_v_outgoing_edges (Graph g, VerticeID id)
{
	return (g_get_vertice (g, id)->outgoing_edge_id);
}

/* Get array of undirected EdgeID(s). */
_ID_TYPE* g_v_undirected_edges (Graph g, VerticeID id)
{
	return (g_get_vertice (g, id)->undirected_edge_id);
}

/* Check if Vertice is isolated or not.
 * Definition: An isolated vertice is a vertice with degree zero; that is, a vertice that is not an endpoint of any edge.
 * -Wikipedia. */
bool g_v_is_isolated (Graph g, VerticeID v)
{
	if (g_v_indegree (g, v) + g_v_outdegree (g, v) > 0)
		return false;
	return true;
}

/* Check if Vertice is leaf to graph or not.
 * Definition: A leaf vertice (also pendant vertice) is a vertice with degree one.
 * -Wikipedia. */
bool g_v_is_leaf (Graph g, VerticeID v)
{
	if (g_get_vertice (g, v)->incoming_edge_count + g_get_vertice (g, v)->outgoing_edge_count + g_get_vertice (g, v)->undirected_edge_count == 1)
		return true;
	return false;
}

/* Check if Vertice is souce vertice or not.
 * Definition: A source vertice is a vertice with indegree zero.
 * -Wikipedia. */
bool g_v_is_source_vertice (Graph g, VerticeID v)
{
	return (g_v_indegree (g, v) == 0 && g_v_outdegree (g, v) != 0);
}

/* Check if Vertice is sink vertice or not.
 * Definition: A sink vertice is a vertice with outdegree zero.
 * -Wikipedia. */
bool g_v_is_sink_vertice (Graph g, VerticeID v)
{
	return (g_v_outdegree (g, v) == 0 && g_v_indegree (g, v) != 0);
}

/* Get Vertice data. */
void* g_v_get_data (Graph g, VerticeID v)
{
	return (g_get_vertice (g, v)->data);
}

/* Set Vertice data.
 * WARNING: This will free old data (if exists). */
void g_v_set_data (Graph g, VerticeID v, void* data)
{
	if (g_get_vertice (g, v)->data != NULL)
	{
		free (g_get_vertice (g, v)->data);
	}
	g_get_vertice (g, v)->data = data;
}
