/*
 * Copyright (C) Arth Patel (PioneerAxon) 2012 <arth.svnit@gmail.com>
 * See README file for licensing information.
 */

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

#include "graph.h"

#define BURST_SIZE 100

/* Get Vertice from VerticeID. */
static Vertice g_get_vertice (Graph g, VerticeID id)
{
	assert (g->vertices_count > id);

	return (&(g->vertices [id]));
}

/* Get Edge from EdgeID. */
static Edge g_get_edge (Graph g, EdgeID id)
{
	assert (g->edges_count > id);

	return (&(g->edges [id]));
}

/* Insert incoming EdgeID to Vertice v. */
static void g_add_incoming_edge (Graph g, Vertice v, Edge e)
{	
	if (v->incoming_edge_count % BURST_SIZE == 0)
	{
		v->incoming_edge_id = (_ID_TYPE*) realloc (v->incoming_edge_id, sizeof (_ID_TYPE) * (v->incoming_edge_count + BURST_SIZE));
		assert (v->incoming_edge_id != NULL);
	}

	v->incoming_edge_id [v->incoming_edge_count] = e->uid;
	v->incoming_edge_count++;
}

/* Insert outgoing EdgeID to Vertice v. */
static void g_add_outgoing_edge (Graph g, Vertice v, Edge e)
{
	if (v->outgoing_edge_count % BURST_SIZE == 0)
	{
		v->outgoing_edge_id = (_ID_TYPE*) realloc (v->outgoing_edge_id, sizeof (_ID_TYPE) * (v->outgoing_edge_count + BURST_SIZE));
		assert (v->outgoing_edge_id != NULL);
	}

	v->outgoing_edge_id [v->outgoing_edge_count] = e->uid;
	v->outgoing_edge_count++;
}

/*Insert undirected EdgeID to Vertice V. */
static void g_add_undirected_edge (Graph g, Vertice v, Edge e)
{
	if (v->undirected_edge_count % BURST_SIZE == 0)
	{
		v->undirected_edge_id = (_ID_TYPE*) realloc (v->undirected_edge_id, sizeof (_ID_TYPE) * (v->undirected_edge_count + BURST_SIZE));
		assert (v->undirected_edge_id != NULL);
	}

	v->undirected_edge_id [v->undirected_edge_count] = e->uid;
	v->undirected_edge_count++;
}

/* Create a graph object. */
Graph g_create_graph ()
{
	Graph new_graph;
	new_graph = (Graph) malloc (sizeof (struct graph_t));
	assert (new_graph != NULL);

	new_graph->vertices = NULL;
	new_graph->vertices_count = 0;
	new_graph->edges = NULL;
	new_graph->edges_count = 0;

	return new_graph;
}

/* Destroy a graph object. */
void g_destroy_graph (Graph g)
{
	_COUNT_TYPE l;
	if (!g)
		return;
	for (l = 0L; l < g->vertices_count; l++)
	{
		if (g->vertices [l].data)
			free (g->vertices [l].data);
		if (g->vertices [l].incoming_edge_count)
			free (g->vertices [l].incoming_edge_id);
		if (g->vertices [l].outgoing_edge_count)
			free (g->vertices [l].outgoing_edge_id);
	}
	for (l = 0L; l < g->edges_count; l++)
	{
		if (g->edges [l].data)
			free (g->vertices [l].data);
	}
	free (g->vertices);
	free (g->edges);
	free (g);
}

/* Create a new vertice into Graph g. */
VerticeID g_insert_vertice (Graph g, void* data)
{
	if (g->vertices_count % BURST_SIZE == 0)
	{
		g->vertices = (Vertice) realloc (g->vertices, sizeof (struct vertice_t) * (g->vertices_count + BURST_SIZE));
		assert (g->vertices != NULL);
	}

	g->vertices [g->vertices_count].uid = g->vertices_count;
	g->vertices [g->vertices_count].data = data;
	g->vertices [g->vertices_count].incoming_edge_id = NULL;
	g->vertices [g->vertices_count].incoming_edge_count = 0;
	g->vertices [g->vertices_count].outgoing_edge_id = NULL;
	g->vertices [g->vertices_count].outgoing_edge_count = 0;
	g->vertices [g->vertices_count].undirected_edge_id = NULL;
	g->vertices [g->vertices_count].undirected_edge_count = 0;
	g->vertices_count++;

	return g->vertices [g->vertices_count - 1].uid;
}

/* Insert a new directed Edge with start VerticeID from and end VerticeID to into Graph g. */
EdgeID g_insert_edge_directed (Graph g, VerticeID from, VerticeID to, void* data)
{
	if (g->edges_count % BURST_SIZE == 0)
	{
		g->edges = (Edge) realloc (g->edges, sizeof (struct edge_t) * (g->edges_count + BURST_SIZE));
		assert (g->edges != NULL);
	}

	g->edges [g->edges_count].uid = g->edges_count;
	g->edges [g->edges_count].type = Edge_Type_Directed;
	g->edges [g->edges_count].data = data;
	g->edges [g->edges_count].from_vertice_id = from;
	g->edges [g->edges_count].to_vertice_id = to;
	g->edges_count++;

	g_add_incoming_edge (g, g_get_vertice (g, to), g_get_edge (g, g->edges_count - 1));
	g_add_outgoing_edge (g, g_get_vertice (g, from), g_get_edge (g, g->edges_count - 1));
	return g->edges [g->edges_count - 1].uid;
}

/* Insert a new undirected Edge with end vertice VerticeID v1, VerticeID v2 into Graph g. */
EdgeID g_insert_edge_undirected (Graph g, VerticeID v1, VerticeID v2, void* data)
{
	if (g->edges_count % BURST_SIZE == 0)
	{
		g->edges = (Edge) realloc (g->edges, sizeof (struct edge_t) * (g->edges_count + BURST_SIZE));
		assert (g->edges != NULL);
	}

	g->edges [g->edges_count].uid = g->edges_count;
	g->edges [g->edges_count].type = Edge_Type_Undirected;
	g->edges [g->edges_count].data = data;
	g->edges [g->edges_count].from_vertice_id = v1;
	g->edges [g->edges_count].to_vertice_id = v2;
	g->edges_count++;

	g_add_undirected_edge (g, g_get_vertice (g, v1), g_get_edge (g, g->edges_count - 1));
	g_add_undirected_edge (g, g_get_vertice (g, v2), g_get_edge (g, g->edges_count - 1));
	return g->edges [g->edges_count - 1].uid;
}

/* Insert a new (directed/undirected) Edge into Graph g. */
EdgeID g_insert_edge (Graph g, VerticeID v1, VerticeID v2, void* data, EdgeType type)
{
	if (type == Edge_Type_Directed)
	{
		return g_insert_edge_directed (g, v1, v2, data);
	}
	if (type == Edge_Type_Undirected)
	{
		return g_insert_edge_undirected (g, v1, v2, data);
	}
	return (_ID_TYPE)0;
}

/* Number of vertices in Graph g. */
_COUNT_TYPE g_vertice_count (Graph g)
{
	return g->vertices_count;
}

/* Number of edges in Graph g. */
_COUNT_TYPE g_edge_count (Graph g)
{
	return g->edges_count;
}

/* Debugging functions. */

void dump_vertice (FILE* stream, Vertice v)
{
	_COUNT_TYPE l;
	fprintf (stream, "Vertice(%p): uid=%lu data=%p incoming=%lu outgoing=%lu undirected=%lu\n", v, v->uid, v->data, v->incoming_edge_count, v->outgoing_edge_count, v->undirected_edge_count);
	fprintf (stream, "\tIncoming   location=%p edges = {", v->incoming_edge_id);
	for (l = 0L; l < v->incoming_edge_count; l++)
	{
		fprintf (stream, "%lu,",v->incoming_edge_id [l]);
	}
	fprintf (stream, "}\n\tOutgoing   location=%p edges = {", v->outgoing_edge_id);
	for (l = 0L; l < v->outgoing_edge_count; l++)
	{
		fprintf (stream, "%lu,",v->outgoing_edge_id [l]);
	}
	fprintf (stream, "}\n\tUndirected location=%p edges = {", v->undirected_edge_id);
	for (l = 0L; l < v->undirected_edge_count; l++)
	{
		fprintf (stream, "%lu,",v->undirected_edge_id [l]);
	}
	fprintf (stream, "}\n\n");
}

void dump_edge (FILE* stream, Edge e)
{
	if (e->type == Edge_Type_Directed)
		fprintf (stream, "Directed   Edge(%p): uid=%lu, data=%p from=%lu to=%lu\n", e, e->uid, e->data, e->from_vertice_id, e->to_vertice_id);
	if (e->type == Edge_Type_Undirected)
		fprintf (stream, "Undirected Edge(%p): uid=%lu, data=%p from=%lu to=%lu\n", e, e->uid, e->data, e->from_vertice_id, e->to_vertice_id);
}

void dump_graph (FILE* stream, Graph g)
{
	_COUNT_TYPE l;
	fprintf (stream, "Graph: vertice count=%lu edge count=%lu\n\n", g->vertices_count, g->edges_count);
	for (l = 0L; l < g->vertices_count; l++)
	{
		dump_vertice (stream, g_get_vertice (g, l));
	}
	for (l = 0L; l < g->edges_count; l++)
	{
		dump_edge (stream, g_get_edge (g, l));
	}
	fprintf (stream, "\n");
}
