/*
 * Copyright (C) Arth Patel (PioneerAxon) 2012 <arth.svnit@gmail.com>
 * See README file for licensing information.
 */

#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#include "graph.h"

/* Get Vertice from VerticeID. */
static Vertice g_get_vertice (Graph g, VerticeID id)
{
	assert (g->vertices_count > id);

	return (&(g->vertices [id]));
}

/* Get Edge from EdgeID. */
static Edge g_get_edge (Graph g, EdgeID id)
{
	assert (g->edges_count > id);

	return (&(g->edges [id]));
}

/* Get Edge type. */
EdgeType g_e_get_type (Graph g, EdgeID id)
{
	return (g_get_edge (g, id)->type);
}

/* Get Edge data. */
void* g_e_get_data (Graph g, EdgeID e)
{
	return (g_get_edge (g, e)->data);
}

/* Set Edge data.
 * WARNING: This will free old data (if exists). */
void g_e_set_data (Graph g, EdgeID e, void* data)
{
	if (g_get_edge (g, e)->data != NULL)
	{
		free (g_get_edge (g, e)->data);
	}
	g_get_edge (g, e)->data = data;
}
