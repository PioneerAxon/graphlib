/*
 * Copyright (C) Arth Patel (PioneerAxon) 2012 <arth.svnit@gmail.com>
 * See README file for licensing information.
 */

#ifndef __GRAPH_H_
#define __GRAPH_H_

#define _ID_TYPE unsigned long int
#define _COUNT_TYPE _ID_TYPE

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef enum edge_type
{
	Edge_Type_Directed,
	Edge_Type_Undirected,
} EdgeType;

struct graph_t
{
	void* data;
	struct vertice_t* vertices;
	_COUNT_TYPE vertices_count;
	struct edge_t* edges;
	_COUNT_TYPE edges_count;
};
typedef struct graph_t* Graph;

struct vertice_t
{
	_ID_TYPE uid;
	void* data;
	_ID_TYPE* incoming_edge_id;
	_COUNT_TYPE incoming_edge_count;
	_ID_TYPE* outgoing_edge_id;
	_COUNT_TYPE outgoing_edge_count;
	_ID_TYPE* undirected_edge_id;
	_COUNT_TYPE undirected_edge_count;
};
typedef struct vertice_t* Vertice;

struct edge_t
{
	_ID_TYPE uid;
	void* data;
	_ID_TYPE from_vertice_id;
	_ID_TYPE to_vertice_id;
	enum edge_type type;
};
typedef struct edge_t* Edge;

typedef _ID_TYPE VerticeID;
typedef _ID_TYPE EdgeID;


/* Graph manipulation functions. */
/* graph.c */
Graph g_create_graph ();
/* WARNING: Data stored in vertices or edges will be destroyed (by free()). */
void g_destroy_graph (Graph);
VerticeID g_insert_vertice (Graph, void*);
EdgeID g_insert_edge (Graph, VerticeID, VerticeID, void*, EdgeType);
EdgeID g_insert_edge_directed (Graph, VerticeID, VerticeID, void*);
EdgeID g_insert_edge_undirected (Graph, VerticeID, VerticeID, void*);
void dump_graph (FILE*, Graph);
_COUNT_TYPE g_vertice_count (Graph);
_COUNT_TYPE g_edge_count (Graph);

/* Vertice definition functions. */
/* vertice_defs.c */
_COUNT_TYPE g_get_vertice_indegree (Graph, VerticeID);
_COUNT_TYPE g_get_vertice_outdegree (Graph, VerticeID);
_ID_TYPE* g_get_vertice_incoming_edges (Graph, VerticeID);
_ID_TYPE* g_get_vertice_outgoing_edges (Graph, VerticeID);
_ID_TYPE* g_get_vertice_undirected_edges (Graph, VerticeID);
bool g_v_is_isolated (Graph, VerticeID);
bool g_v_is_leaf (Graph, VerticeID);
bool g_v_is_source_vertice (Graph, VerticeID);
bool g_v_is_sink_vertice (Graph, VerticeID);
void* g_v_get_data (Graph, VerticeID);
void g_v_set_data (Graph, VerticeID, void*);

/* Edge definition functions. */
/* edge_defs.c */
EdgeType g_e_get_type (Graph, EdgeID);
void* g_e_get_data (Graph, VerticeID);
void g_e_set_data (Graph, VerticeID, void*);

#endif /*__GRAPH_H_*/
